#!/usr/bin/env bash

kaggle competitions download -c plant-seedlings-classification
unzip train.zip
unzip test.zip
