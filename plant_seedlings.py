import numpy as np
import torch
import torchvision
import torch.optim as optim
import torch.nn.functional as F
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import torchvision as torchvision

from torch import nn
from torch.utils.data import SubsetRandomSampler
from torchvision.models import resnet34


def prepare_loader(folder, batch_size=32, shuffle=True, num_workers=4, pin_memory=True, valid_size=0.2):
    # Define normalization
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    max_image_size = 224

    # Define transformations
    train_transformations = transforms.Compose([
        transforms.Resize(max_image_size),
        transforms.RandomCrop(max_image_size),
        transforms.RandomHorizontalFlip(),
        transforms.RandomVerticalFlip(),
        transforms.RandomRotation(45),
        transforms.ToTensor(),
        normalize,
    ])
    valid_transformations = transforms.Compose([
        transforms.ToTensor(),
        normalize,
    ])

    # Define datasets
    dataset = datasets.ImageFolder(root=folder, transform=train_transformations)

    num_train = len(dataset)
    indices = list(range(num_train))
    split = int(np.floor(valid_size * num_train))

    random_seed = 42

    if shuffle:
        np.random.seed(random_seed)
        np.random.shuffle(indices)

    train_idx, valid_idx = indices[split:], indices[:split]
    train_sampler = SubsetRandomSampler(train_idx)
    valid_sampler = SubsetRandomSampler(valid_idx)

    # Load images and label
    train_loader = torch.utils.data.DataLoader(
        dataset=dataset, batch_size=batch_size,
        num_workers=num_workers, pin_memory=pin_memory, sampler=train_sampler)

    valid_loader = torch.utils.data.DataLoader(
        dataset=dataset, batch_size=batch_size,
        num_workers=num_workers, pin_memory=pin_memory, sampler=valid_sampler)
    return train_loader, valid_loader


use_cuda = torch.cuda.is_available()
print('Cuda available: ' + str(use_cuda))
print('Number of cuda devices: ' + str(torch.cuda.device_count()))
device = torch.device("cuda:0" if use_cuda else "cpu")

net = resnet34(pretrained=True).to(device)

# Prepare data
train_loader, valid_loader = prepare_loader(folder='train')

# Transfer learning

# Freeze layers
for param in net.parameters():
    param.requires_grad = False

# Replace fully connected layer
num_features = net.fc.in_features
net.fc = nn.Linear(num_features, 12).to(device)

criterion = nn.CrossEntropyLoss()

# Observe that all parameters are being optimized
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

# Run the actual training
epochs = 20000
for i in range(epochs):
    for images, labels in train_loader:
        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        images, labels = images.to(device), labels.to(device)
        outputs = net(images)
        _, prediction = torch.max(outputs, 1)
        correct = [x == y for x, y in zip(labels, prediction)]
        accuracy = correct.count(True) / len(correct)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        # print statistics
        print(f'Loss: {loss.item()}, Accuracy: {accuracy}')
